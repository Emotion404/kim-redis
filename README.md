①redis分布式锁

    测试：
    localhost:20101/v1/redisbody/test
    可多线程访问
    
    
②redis限流

    测试:
    localhost:20103/limit/limitTest1
    多次访问会限流
    
③redis过期监听

    启动kim-redis-expiration-notice项目即可，启动后会监听database下的key,过期会打印日志
    
④mybatis-plus使用redis作为二级缓存

    在mapper上加上注解@CacheNamespace
    @CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)