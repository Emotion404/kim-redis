package com.kim.redis.common.lock.core;

import com.kim.redis.common.lock.annotation.KimLock;
import com.kim.redis.common.lock.model.KimLockModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;

import java.util.UUID;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.redis.common.lock.core
 * @FileName: KimLockModelProvider.java
 * @Description: 获取锁的信息
 * @Author: kimwu
 * @Time: 2020-12-19 14:15:22
 */
@Slf4j
public class KimLockModelProvider {

    static ThreadLocal threadLocal = new ThreadLocal();

    public static KimLockModel get(JoinPoint joinPoint, KimLock kimLock) {
        if (threadLocal.get() == null) {
            if (StringUtils.isNotBlank(kimLock.value())) {
                threadLocal.set(kimLock.value());
            } else {
                threadLocal.set(UUID.randomUUID().toString());
            }
        }
        return new KimLockModel(kimLock.name(), threadLocal.get().toString(), getExpiteTime(kimLock));
    }


    public static Integer getExpiteTime(KimLock lock) {
        return lock.expireTime() == Integer.MIN_VALUE ?
                60 : lock.expireTime();
    }
}
