package com.kim.redis.common.limit.Aspect;

import com.google.common.collect.ImmutableList;
import com.kim.redis.common.limit.Enum.LimitType;
import com.kim.redis.common.limit.Interface.Limit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.lang.reflect.Method;


/**
 * @description 限流切面实现
 */
@Aspect
@Configuration
@Slf4j
public class LimitInterceptor {

    private static final String UNKNOWN = "unknown";

    private final RedisTemplate<String, Object> limitRedisTemplate;

    @Autowired
    public LimitInterceptor(RedisTemplate<String, Object> limitRedisTemplate) {
        this.limitRedisTemplate = limitRedisTemplate;
    }

    /**
     * 个人觉得比较好的写法
     */
    //截获标有@CacheRemove的方法
    @Pointcut(value = "(execution(public * *(..)) && @annotation(com.kim.redis.common.limit.Interface.Limit))")
    private void pointcut() {
    }

    /**
     * @param pjp
     * @description 切面
     * <p>
     * 传入参数 ProceedingJoinPoint  环绕通知 ProceedingJoinPoint 执行proceed方法的作用是让目标方法执行
     * Proceedingjoinpoint 继承了 JoinPoint 。是在JoinPoint的基础上暴露出 proceed 这个方法。proceed很重要，这个是aop代理链执行的方法。
     * <p>
     * 参考网址 https://www.cnblogs.com/zhjh256/p/10694165.html  以后有时间 等整个专场，好好品品
     */
    @Around("pointcut()")
    public Object interceptor(ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        Limit limitAnnotation = method.getAnnotation(Limit.class);
        LimitType limitType = limitAnnotation.limitType();
        String name = limitAnnotation.name();
        String key;
        int limitPeriod = limitAnnotation.period();
        int limitCount = limitAnnotation.count();
        String describle = limitAnnotation.describle();
        /**
         * 根据限流类型获取不同的key ,如果不传我们会以方法名作为key
         */
        switch (limitType) {
            /** 可扩展 因为我们之前拓展了 Limit */
            case IP:
                key = getIpAddress();
                break;
            case CUSTOMER:
                key = limitAnnotation.key();
                break;
            default:
                key = StringUtils.upperCase(method.getName());
        }

        ImmutableList<String> keys = ImmutableList.of(StringUtils.join(limitAnnotation.prefix(), key));
        try {
            /** 对应lua语句  轻量小巧的脚本语言 目的为了嵌入应用程序中  对应lua语法进行简单解析了*/
            String luaScript = buildLuaScript();
            /** 这里可能理解不太到位  对应应该是将lua转换成RedisScript 类型  然后后续可以进行执行 */
            RedisScript<Number> redisScript = new DefaultRedisScript<>(luaScript, Number.class);
            /** 对应keys   以及 limitCount跟limitPeriod  是 Object... args */
            Number count = limitRedisTemplate.execute(redisScript, keys, limitCount, limitPeriod);
            /** 后续是正常的数据 正常的java逻辑就可以进行梳理 */
            log.info("Access try count is {} for name={} and key = {}", count, name, key);
            if (count != null && count.intValue() <= limitCount) {
                return pjp.proceed();
            } else {
                throw new RuntimeException("You have been dragged into the blacklist");
            }
        } catch (Throwable e) {
            throw new RuntimeException(describle);
        }
    }

    /**
     * @description 编写 redis Lua 限流脚本  对应执行lua 对应包是在 org.springframework.data.redis
     * <p>
     * 首先 lua语言比较像 C 所以习惯起来还是不是很难的
     * local c   创建一个C的局部变量  变量应该是local ,不可以是全局变量，否则会报错
     * \n 是回车  c = redis.call('get',KEYS[1])   这里主要是理解 KEYS[1] 跟 redis.call()
     * redis.call()  是Lua脚本调用Redis的Lua函数  另外一个是 redis.pcall()
     * 二个区别是出错，redis.call()返回调用者一个错误;redis.pcall()  对应错误以Lua表的形式返回
     * KEYS[1] 用来表示在redis中用作键值的参数占位，主要是用来传递redis中用作keys值的参数
     * if c and tonumber(c) > tonumber(ARGV[1]) then   【Lua 中 0为true】 【ARGV[1]是返回值的VALUE值】
     * return c;  返回对应值
     * end  结束语
     * c = redis.call('incr',KEYS[1])  同理 对应是自增的KEYS 1
     * if tonumber(c) == 1 then   //tonumber 对应是转换成数字类型的，因为redis出来的可能是String类型
     * redis.call('expire',KEYS[1],ARGV[2])  对应放置过期时间对应某个值放第二个对应时间的值
     * end
     * return c;
     */
    public String buildLuaScript() {
        StringBuilder lua = new StringBuilder();
        lua.append("local c");
        lua.append("\nc = redis.call('get',KEYS[1])");
        // 调用不超过最大值，则直接返回
        lua.append("\nif c and tonumber(c) > tonumber(ARGV[1]) then");
        lua.append("\nreturn c;");
        lua.append("\nend");
        // 执行计算器自加
        lua.append("\nc = redis.call('incr',KEYS[1])");
        lua.append("\nif tonumber(c) == 1 then");
        // 从第一次调用开始限流，设置对应键值的过期
        lua.append("\nredis.call('expire',KEYS[1],ARGV[2])");
        lua.append("\nend");
        lua.append("\nreturn c;");
        return lua.toString();
    }


    /**
     * @description 获取id地址  获取IP 不过对应如果是通过NGIX的话 还需要做一些转换。以及有一些会跳转多次，要还原出最初调用的IP
     */
    public String getIpAddress() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}