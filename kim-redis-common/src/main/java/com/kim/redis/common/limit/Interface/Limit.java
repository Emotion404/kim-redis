package com.kim.redis.common.limit.Interface;

import com.kim.redis.common.limit.Enum.LimitType;

import java.lang.annotation.*;

/**
 * @description 自定义限流注解
 * 理解  这里是对应的一些值注解。这部分参考网上的AOP的创建，对应资料就比较多了
 * 跟枚举类
 * @see LimitType  存在联动
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Limit {

    /**
     * 名字
     */
    String name() default "";

    /**
     * key
     */
    String key() default "";

    /**
     * Key的前缀
     */
    String prefix() default "";

    /**
     * 给定的时间范围 单位(秒)
     */
    int period();

    /**
     * 一定时间内最多访问次数
     */
    int count();

    /**
     * 限流的类型(用户自定义key 或者 请求ip)
     */
    LimitType limitType() default LimitType.CUSTOMER;

    /**
     * 限流后返回描述 可以是准确提示:比如 某个IP，对应访问次数超过了XX次数;如果对外，就说繁忙，稍后再试，委婉拒绝就可以了
     */
    String describle() default "当前系统繁忙，请稍后再试";
}