package com.kim.redis.common.lock.annotation;


import java.lang.annotation.*;

/**
 * 加锁的注解
 */

@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface KimLock {

    /**
     * 锁的名称
     */
    String name() default "";

    /**
     * 锁的value
     */
    String value() default "";

    /**
     * 过期时间(如果不设置默认60s)
     */
    int expireTime() default 60;


}
