package com.kim.lock.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.handle.server
 * @FileName: RedisLockApplication.java
 * @Description: The RedisLockApplication is...
 * @Author: kimwu
 * @Time: 2020-12-18 14:38:59
 */
@SpringBootApplication
@EnableAspectJAutoProxy  //开启spring对aspect的支持
public class RedisLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisLockApplication.class, args);
    }
}
