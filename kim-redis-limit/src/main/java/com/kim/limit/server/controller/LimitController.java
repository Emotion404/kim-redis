package com.kim.limit.server.controller;

import com.kim.redis.common.limit.Interface.Limit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.limit.server.controller
 * @FileName: LimitController.java
 * @Description: The LimitController is...
 * @Author: kimwu
 * @Time: 2020-12-25 10:38:36
 */
@RestController
@RequestMapping("/limit")
public class LimitController {

    private static final AtomicInteger ATOMIC_INTEGER_1 = new AtomicInteger();

    /**
     * @description
     */
    @Limit(key = "limitTest", period = 10, count = 3,describle = "限制访问")
    @GetMapping("/limitTest1")
    public int testLimiter1() {

        return ATOMIC_INTEGER_1.incrementAndGet();
    }
}
