package com.kim.redis.expiration.notice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Project: kim-redis
 * @PackageName: com.kim.redis.expiration.notice
 * @FileName: NoticeApplication.java
 * @Description: The NoticeApplication is...
 * @Author: kimwu
 * @Time: 2020-12-19 14:01:56
 */
@SpringBootApplication
public class NoticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoticeApplication.class, args);
    }

}
