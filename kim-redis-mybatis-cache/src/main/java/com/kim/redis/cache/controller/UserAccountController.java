package com.kim.redis.cache.controller;


import com.kim.redis.cache.entity.UserAccount;
import com.kim.redis.cache.service.IUserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户账户信息表 前端控制器
 * </p>
 *
 * @author KimWu
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/userAccount")
@Slf4j
public class UserAccountController {

    @Autowired
    private IUserAccountService userAccountService;

    @PostMapping("/all")
    public String all() {
        List<UserAccount> list =
                userAccountService.list();
        log.info(list.toString());
        return "ok";
    }

}
