package com.kim.redis.cache.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kim.redis.cache.entity.UserAccount;
import com.kim.redis.cache.mapper.UserAccountMapper;
import com.kim.redis.cache.service.IUserAccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户信息表 服务实现类
 * </p>
 *
 * @author KimWu
 * @since 2020-12-25
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements IUserAccountService {

}
