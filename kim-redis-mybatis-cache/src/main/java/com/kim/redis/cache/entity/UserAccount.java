package com.kim.redis.cache.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户账户信息表
 * </p>
 *
 * @author KimWu
 * @since 2020-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_account")
public class UserAccount implements Serializable {

    /**
     * 主键id
     */
    @TableId("id")
    private Long id;

    /**
     * 用户名（微信号），只能设置一次
     */
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 电话号
     */
    @TableField("phone")
    private String phone;

    /**
     * 是否启用 1:启用 0:未启用
     */
    @TableField("is_enabled")
    private Boolean isEnabled;

    /**
     * 是否锁定 0:锁定 1:未锁定
     */
    @TableField("is_unlocked")
    private Boolean isUnlocked;

    /**
     * 是否过期 0：过期 1：未过期
     */
    @TableField("is_unExpired")
    private Boolean isUnexpired;

    /**
     * 是否删除 0：未删除 1：删除
     */
    @TableField("is_delete")
    private Boolean isDelete;

    /**
     * 创建时间
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @TableField("update_time")
    private LocalDateTime updateTime;


}
